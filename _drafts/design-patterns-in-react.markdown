---
title: "Design Patterns in React.js "
date: "2018-11-13"
categories: javasript
author: Gurjit Singh
image: "/publicImages/generator.jpeg"
comments: true
keywords: "javascript, es6, es6-generators"
---

Since the launch of **React.js**, it has gain tremendous heights and have been one of the hot topics in front end development. `Components`, `JSX syntax`, `minimal API`, `solid community` etc all entices us to atleast give a shot to this wonderful library at some point or another.

Throughout these 5 years, many developers working over **React.js** have developed so many design patterns to make
