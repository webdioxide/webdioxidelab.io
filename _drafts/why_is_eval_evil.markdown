---
title:  "YDKJS: Evils of eval( )"
date:  "2018-10-20"
categories: javasript
author: Ishwerdas
image: "/publicImages/eval-is-evil.jpeg"
comments: true
keywords: "javascript, eval, eval is bad, evils of eval, tips"
---

My brother has a wise saying. He says "Nothing is good or bad, it's good and Bad". I love that. It's such a crisp way of understanding the zen principle of yin and yang. 

So before I go on and write down evils of eval, before you go ahead and contradict my evaluation of evilness of eval, let me assure you that I do understand the fact that there are some legit ways to use eval and this post is more about understanding how eval gets evil rather than asserting that eval is always evil.

## Cheating lexical scope
One of the careless side effects of using eval can be that you end up cheating the lexical scope of a variable. In the code below variable named `snake` was meant to stay outside the scope of the function `well()` , however when we send a string that has the same variable name as in the function well, it just overrides the global `snake` and resorts to the `snake` that we define inside evilStr.  

```javascript
function well(evilStr, frog) {
    eval( evilStr ); // cheats lexical scope and makes snake "hangry"
    console.log( frog, snake ); // trr trr hangry
}

var snake = "sleeping";

foo( "var snake = 'hangry';", "trr trr" ); // 1 3
```

We can use "use strict" to prevent this. With usage strict, eval will have its own lexical scope and the variables won't leak outside.

## It slows down the code
In the compilation phase, the Javascript engine would perform a bunch of optimizations. These optimizations depend upon fixed-in-stone author time lexical scoping. In simpler words, one of those optimizations is to keep track of which identifier is declared at what place. When the engine sees that user is using eval( ), it's no longer sure of what variables and functions would come up in that eval( ). Hence, the engine has to abandon at least some of its optimizations and opt for a slower code.

Sometimes and I believe those times are rare, you are okay sacrificing that performance. Which is okay, I guess! As long as you know what you are doing.

## It's not secure
If a bad or malicious code ever finds its way into eval( ). It opens up your code to injection attacks. So needless to say no user input should be using eval( ). Even otherwise, one should be careful. 

## It's hard to code and debug
The code you write inside eval does not have line numbers or formatting of any kind. You cannot take advantage of your editor features, or linting rules. (Assuming you decided to override the linting rule  of `no-eval`). It's just a blob of string and especially when it contains more than a couple of JS statements, it gets ugly. 

That's as much slander as I can afford for today. For beginners or for cautious, use eval( ) only when you are out of all other options. In almost all the cases you can avoid eval( ) and it's evils. 