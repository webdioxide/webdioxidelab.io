import '../index.scss';
import 'prismjs/themes/prism-tomorrow.css';

import { graphql, useStaticQuery } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import { Header } from './Layout/Header';

interface LayoutProps {
  children: React.ReactNode;
}

export const Layout = ({ children }: LayoutProps) => {
  const query = useStaticQuery(graphql`
  query TemplateWrapperQuery {
    site {
      siteMetadata {
        title
      }
    }
  }`);
  return (
    <div>
      <Header title={query?.site?.siteMetadata?.title} />
      {children}
    </div>
  );
};
Layout.propTypes = {
  children: PropTypes.element.isRequired,
};
