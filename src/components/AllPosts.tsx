import { Link } from 'gatsby';
import React from 'react';

interface IProps {
  posts: {
    node: {
      id: string;
      fields: {
        slug: string;
      };
      frontmatter: {
        title: string;
        date: string;
      }
    }
  }[];
  featured: boolean;
}

const AllPosts = (props: IProps) => {
  const { posts, featured } = props;
  return (
    <div className={`all-posts ${featured ? 'featured' : ''}`}>
      <h3 className="sidebar-heading"> More Posts </h3>
      {posts.map(({ node: { id, fields: { slug }, frontmatter: { title, date } } }) => (
        <div className="post" key={id}>
          <Link
            activeStyle={{ textDecoration: 'none', color: 'inherit' }}
            to={slug}
          >
            <div>
              <p>
                {title}{' '}
              </p>
              <span>— {date}</span>
            </div>
          </Link>
        </div>
      ))}
    </div>
  );
};

export default AllPosts;
