import { Link } from 'gatsby';
import React from 'react';

interface IProps {
  postLink: string;
}

const Button = (props: IProps) => {
  const { postLink } = props;
  return (
    <Link
      activeStyle={{ textDecoration: 'none', color: 'inherit' }}
      to={postLink}
    >
      <button className="solid-btn" type="button">Read More ...</button>
    </Link>
  );
};

export default Button;
