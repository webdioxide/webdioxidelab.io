---
layout: post
title:  "YDKJS: Scope & Closures Supersheet (quiz)"
date: "2018-10-29"
categories: Javascript
author: Inder Singh
image: "/publicImages/questions.jpeg"
keyword: "Javascript, scope, closures, hoisting"
comments: true
---

When I was in school, a week before exams, my mother used to make something she called "super sheets". Super sheets as she would claim **guaranteed my success in exam**. Today, I have created a super sheet for the  *scope & closures*, the second book in YDKJS book series.

Super sheets are basically a big list of questions that cover every nook and corner of the subject. The idea is that after you have read the book, instead of revising the book by reading it again, you would just go through questions and add a tick in front of the ones that you can answer and cross in front of the ones that you cannot. Thus, giving you a quantitative, more clear assessment of which topics you still need to cover. It's simple but it works rather immaculately. 

Yes, of course, there are other ways to test your understanding, but I haven't personally found a way as quick and as efficient as super sheets yet. It works for me, maybe it works for you.

So get set, sharpen your pencils, and GO!!

## What is Scope?
* What gives the program a state?
* Can you define scope?
* Is Javascript a compiled language? or What makes Javascript a compiled language?
* What are 3 steps of compilation? Can you briefly define what each of those steps does?
* What steps will JS engine take while processing the statement `var a = 2`?
* What is LHS and RHS lookup?
* RHS is not strictly about right-hand side of the assignment, it's even rephrased as "Retrieve his/her source". Can you explain why?
* Identify all LHS and RHS lookups in the following code

```javascript
function foo(a) {
    var b = a;
    return a + b;
}
var c = foo( 2 );
```

* Nested lookup is compared to an elevator going up with the top floor being global scope, can you explain why?
* What error do you get when RHS lookup fails?
* What happens when LHS lookup fails?
* How does strict mode effect LHS lookup failure?
* What error do you get when you try to do something illegal with a variable?

## Lexical Scope
* Name two types of scopes in programming languages.
* Define the lexical scope.
* Can you create scope bubbles in the following code?

```javascript
function foo(a) {

    var b = a * 2;

    function bar(c) {
        console.log( a, b, c );
    }
    bar(b * 3);
}
foo( 2 ); // 2 4 12
```

* Scope bubbles are not like Venn diagrams, how?
* What is shadowing? How is look up affected by shadowing? (*Scope look-up stops once it finds the first match.*)
* If a global variable is shadowed in the nested scope. How would you access the value of the global variable from the nested scope? (*`window.varName`*)
* What is eval?
* Can you write a code that cheats lexical scope via eval?
* How does eval behaviour change under strict mode?
* How are `setTimeout`, `setInterval`, and `new Function` related to eval in the context of cheating lexical scope?
* What is `with` in javascript?
* no more questions on with as it's deprecated` 
* What makes `eval` evil on performance?

## Function v/s Block Scope
>The traditional way of thinking about functions is that you declare a function, and then add code inside it. But the inverse thinking is equally powerful and useful: take any arbitrary section of code you've written, and wrap a function declaration around it, which in effect "hides" the code.
* Why would "hiding" variables and functions be a useful technique?
* Define principle of least privilege.
* How can hiding avoid the collision in identifiers?
* What are "global namespaces"?
* What are disadvantages of having anonymous function expressions?
* What does IIFE stand for?
* What is the syntax for IIFE?
* Javascript has function as a scope but with a couple of exceptions. What are those exceptions?
* What is different (in context of scope) between creating a variable with `var` v/s creating a variable with `let`?
* What is explicit block?
* How is `let` different from `var`, when it comes to hoisting?
* How block scope helps in better garbage collection?
* How is `let` different from `var` when used in loops? or Benefit of let in loops
* What is `const`?

## Hoisting
* Consider this code

```javascript
a = 2;
var a;
console.log( a );
```

What do you expect to be printed? and why?
* Consider this code

```javascript
console.log( a );
var a = 2;
```

What do you expect to be printed? and why?
* Define hoisting.
* Is hoisting per scope? 
* Are function declarations hoisted?
* Are function expressions hoisted?
* Between function and variables which are hoisted first?
* consider this code:

```javascript
foo(); // "b"
var a = true;
if (a) {
   function foo() { console.log( "a" ); }
}
else {
   function foo() { console.log( "b" ); }
}
```

What do you expect to be printed? and why?
* Why shouldn't we define functions inside blocks?

## Closures
* Define closure.
* Find the closure in the code below. Is it observable closure?

```javascript
function foo() {
    var a = 2;

    function bar() {
        console.log( a ); // 2
    }

    bar();
}
foo();
```

* Point the observable closure in code below.

```javascript
function foo() {
    var a = 2;

    function bar() {
        console.log( a );
    }

    return bar;
}
var baz = foo();
baz();
```

* Find closures in the code snippets below

```javascript
function foo() {
    var a = 2;

    function baz() {
        console.log( a ); 
    }
    bar( baz );
}
function bar(fn) {
    fn(); 
}
```

```javascript
var fn;
function foo() {
    var a = 2;

    function baz() {
        console.log( a );
    }

    fn = baz; 
}
function bar() {
    fn(); 
}
foo();
bar(); 
```

```javascript
function wait(message) {
    setTimeout( function timer(){
       console.log( message );
    }, 1000 );
}
wait( "Hello, world!" );
```

* Would you consider `IIFE` as an observed closure, if not, why?
* The output of code below is `6` is printed 5 times. Why is that? Can you get the output as `1 2 3 ... 5` in the code below without using let?

```javascript
for (var i=1; i<=5; i++) {
    setTimeout( function timer(){
        console.log( i );
    }, i*1000 );
}
```

* Write a module in javascript without using ES6.
* How are modules used in ES6?
> Function-based modules aren't a statically recognized pattern
* What does above statement mean? How is it different from ES6 modules?

Phew! I hope you found that at-least interesting if not a better way to revise the YDKJS book.