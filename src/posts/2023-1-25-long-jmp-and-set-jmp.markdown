---
title: "Longjmp and setjmp in C"
date: "2023-01-25"
categories: c
author: Anmolpreet Kaur
image: "/publicImages/coffee-chill.jpg"
comments: true
keywords: "setjmp, longjmp, c-tips, c, c++, errors"
---

Making mistakes is part of being human. We often wonder if we could go to "that" particular moment in past and start our life all over again from there.
But retracing one's steps in the face of adversity, to correct a misstep, is a luxury not often afforded to us in life.
However, in the realm of programming, the ability to revisit past decisions and rectify errors is possible through setjmp and longjmp.

longjmp and setjmp are C standard library functions that are used to perform non-local jumps in a program's execution flow.
These functions are typically used in situations where it is necessary to jump out of multiple levels of nested function calls or
when dealing with errors that need to be handled in an efficient and organized manner.

The syntax for the setjmp function in C is as follows:

```c
int setjmp(jmp_buf env);
```

where `env` is a variable of type `jmp_buf`, which is used to store the current execution context.
The `setjmp` function saves the current execution context in the env variable and returns 0.


The syntax for the `longjmp` function in C is as follows:

```c
void longjmp(jmp_buf env, int val);
```
where env is a variable of type jmp_buf that was previously set by a call to setjmp, and val is an integer value that is returned by the
setjmp call when longjmp is executed.

An example could be working with low-level system calls or libraries, where returning error codes may not be an option or it's not efficient,
in this scenario you could use setjmp and longjmp to handle such errors.

```c
#include <setjmp.h>
#include <stdio.h>
#include <unistd.h>

jmp_buf buf;

void low_level_call() {
    // This is a low-level system call that does not return error codes
    int result = syscall(SYS_getpid);
    if (result < 0) {
        longjmp(buf, 1);
    }
    printf("The process ID is: %d\n", result);
}

int main() {
    if (setjmp(buf) == 0) {
        low_level_call();
    } else {
        printf("Error: low_level_call failed!\n");
    }
    return 0;
}
```
In this example, the `low_level_call` function makes a low-level system call using the syscall function. This call doesn't return an error code, so the function checks the returned value and if it's less than zero,
it uses longjmp to jump back to the setjmp call in the main function, passing the value 1 as the argument. This causes the program to jump back to the location where setjmp was called, and the value 1 is returned.
In this case, the if statement evaluates to true, and the program continues to execute the error handling code, in this case, printing "Error: low_level_call failed!"

This is a simple example, in a real-world scenario, you should check the man page or the documentation of the library or system call you are using to check if it returns error codes or not,
and also you should use more robust error handling techniques like exceptions or error codes, which are more widely used in modern C.

It's important to note that setjmp and longjmp should be used with care, as they can have unexpected side effects on the program execution flow if not used properly.
They should be considered as a last resort, when there are no better options available, and also you should use them with caution when working with low-level system calls, where the behavior of the program can be unexpected.
