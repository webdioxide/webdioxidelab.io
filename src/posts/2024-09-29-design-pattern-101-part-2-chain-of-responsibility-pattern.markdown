---
layout: post
title: "Design pattern 101 (Part 2): CoR, the Chain of Responsibility pattern"
date: "2024-08-20"
categories: typescript
author: Shiv Charan Sharma
image: "/publicImages/design-pattern-101.png"
comments: true
keywords: "Design Patterns, Low level design, Coding Best Practices, Software Engineering, Chain of Responsibility"
---

What's up? programmers!

This is part 2 of our design pattern series. And your next step to be a seasoned developer.

In this part, we will learn CoR the Chain of Responsibility pattern.

Sooo.. are you ready to take responsibility for learning this?

I hear yes, so let's gooo…

### What the heck is CoR in general terms?

It is a behavioral design pattern in which you can pass a given request or data over a chain of handlers, where each handler can decide if it should process the request or pass it to the next handler.

### Let's learn that with an example.

Have you gone through any interview process yet? if not…

In the interview process, you will go through various steps.

Telephone interview → Online test → Coding round 1 → Coding round 2 → HR round

The interview process evaluates various skills. Instead of testing everything at once, each step focuses on specific skills.

- i.e. the responsibility of testing specific skills is divided into multiple steps or handlers.

**P.S. The number of steps/tests can vary based on the job role**

Let the seniors feel all the pain of more rounds, why should an intern give 2 coding rounds 😉

#### Now what are the traits of handlers we discussed in the interview process

- At each step know, if it needs to handle you or can directly pass you to the next stage.
- What to test (specific task of handler)
- Should you even be allowed to go to the next stage 🤨 or end the process here

Now you know what is CoR or at least the interview process 😂

### Why are you learning this? Let's see,

Have you seen any similar Chain of responsibility while developing software????

Let me help you

If you are working on the backend

You probably auth layer and middleware before actually handling the API request. If the request is not authorized it does not reach the next stage.

Same in the frontend, click event on any element bubble up through all the parent dom element and the element having appropriate handler will handle the request.

### How can you use CoR when you are implementing the next related feature?

Let me help you with the above example in 3 easy steps.

1. Need to create a base handler with methods as shown in the following code

   ```tsx
   class Handler {
     next: Handler;

     setNext(h: Handler) {
       this.next = h;
       return this.next;
     }
     handle(request) {
       // request contains candidate data
       // Common logic to handle if any
       if (this.next !== null) {
         this.next.handle(request);
       }
     }
   }
   ```

2. Create a separate handler based on the interview process that extend the base handler

   ```tsx
   class AptitudeTestHandler extend Handler {
     handle(request) {
       // evaluate Aptitude test metrics
       // Return from here if failed
       if (this.next !== null) {
         this.next.handle(request);
       }
     }
   }

   class CodingTestHandler extend Handler {
     handle(request) {
       /* canHandle can have logic to check
         if a coding test is needed for a candidate
         based on tech or non-tech job role
       */
       if (canHandle(request)){
           // evaluate coding test  metrics
           // Return from here if failed
       }

       if (this.next !== null) {
         this.next.handle(request);
       }
     }
   }

   class HrRoundHandler extend Handler {
     handle(request) {
       // request contain candidate data
       // Evaluate behavior Q/A metrics
       // Return from here if failed
       if (this.next !== null) {
         this.next.handle(request);
       }
     }
   }

   class SelectionHandler extend Handler {
     handle(request) {
       // Request contain candidate data
       // Handle informing user for selection
       if (this.next !== null) {
         this.next.handle(request);
       }
     }
   }
   ```

3. Now just need to create CoR with these handler

   ```tsx
   // Now Chain of Responsibility can be built as follows for the interview process
   const aptitudeTestHandler = new AptitudeTestHandler();
   const codingTestHandler = new CodingTestHandler();
   const hrRoundHandler = new HrRoundHandler();
   const selectionHandler = new SelectionHandler();
   aptitudeTestHandler
     .setNest(codingTestHandler)
     .setNext(hrRoundHandler)
     .setNext(selectionHandler);
   // Now Just need to candidate date as requested on the first step
   aptitudeTestHandler(candidateData);
   // It will handle the evaluation at various setups based on the requirement
   ```

And done ✅ Our CoR is ready for action.

### Conclusion

We hope you have learned about the Chain of Responsibility pattern!

Please let us know in the comments if you have queries about the same
Also, we took the responsibility to help you enhance your tech skills

Now it's your responsibility is to learn and subscribe to us.

Only then this Chain of Responsibility be complete. 😂

Stay tuned for the next part of the series 🎯

Peace out!
