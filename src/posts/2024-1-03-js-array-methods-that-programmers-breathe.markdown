---
layout: post
title:  "JS Array methods that programmers breathe"
date: "2024-01-03"
categories: javascript
author: Shiv Charan Sharma
image: "/publicImages/coffee-chill.jpg"
comments: true
keywords: "javascript, array"
---

What's up? programmers!
As you know, array methods are a very essential part of programming. 

Javascript provides us with a wide variety of methods to perform various operations on an array.

Selecting and using the correct array method based on our needs can make a developer's life a lot easier and make the dev more productive.

So, let's start our journey with a brief overview of some must-know JS array methods.

### 1. filter

  It creates a new array (shallow copy) from the given array by filtering down elements that pass test conditions implemented in the provided callback function.
  
  ```javascript
  array.filter((element, index, array) => { /* … */ } )
  ```
  **Uses:**

  Example 1: 
  
  ![filter Example 1](https://cdn-images-1.medium.com/max/1600/1*RtTqmVCG1Q441ALuQNaYRQ.png)

  Example 2: 
  
  ![filter Example 2](https://cdn-images-1.medium.com/max/800/1*utvajGOq9K732uqiMLLkQA.png)

### 2. some

  It returns true, if at least one element passes the test implemented in the given callback function, else returns false
  
  ```javascript
  array.some((element, index, array) => { /* … */ } )
  ```
  **Uses:**
  
  Example 1: 
  
  ![some Example 1](https://cdn-images-1.medium.com/max/800/1*E0uQwrnq2zn8B008E4XnTg.png)

  Example 2: 
  
  ![some Example 2](https://cdn-images-1.medium.com/max/800/1*vdd6JRo_3TF3uIbD9S6aFg.png)

### 3. every

  It returns true if, all the elements pass the test implemented by the provided callback function, Otherwise false
  
  ```javascript
  array.every((element, index, array) => { /* … */ } )
  ```
  **Uses:**
  
  Example 1: 
  
  ![every Example 1](https://cdn-images-1.medium.com/max/800/1*a7qsnmR496UPe1jYHAKx5w.png)

  Example 2: 
  
  ![every Example 2](https://cdn-images-1.medium.com/max/800/1*Hz51r3EwIyLfZhuQMKS4NA.png)

### 4. includes

  It returns true if provided elements exist in the array, Otherwise false
  
  ```javascript
  array.includes(searchElement, fromIndex)
  ```
  **Uses:**
  
  ![includes Example 1](https://cdn-images-1.medium.com/max/800/1*A8iWAn6cU3xRaWpcetU9Iw.png)

  For Object, you should use *`find`* or another similar method for checking given object exists in the array or not

  Reason: As the `includes` function compares objects by reference like `obj1 === obj2`,

  As we pass an object as an argument in includes it passes a new object reference for comparison. Thus, even though the passed object contains values similar to one of the existing elements, but has a different reference, it causes a mismatch, and `myArray.includes(obj)` returns a false value. 
  
  see the following example.

  ![includes with object](https://cdn-images-1.medium.com/max/800/1*ATnem9PsJuCPUlyeO8Xt0A.png)
  
### 5. map

  It creates a new array after calling the given callback function for each element of the given array.
  
  ```javascript
  array.map((element, index, array) => { /* … */ })
  ```
  **Uses:**
  
  Example 1: 
  
  ![map Example 1](https://cdn-images-1.medium.com/max/800/1*YqFHno_VlipdHL_zALCycA.png)

  Example 2: 
  
  ![map Example 2](https://cdn-images-1.medium.com/max/800/1*Ru0CF5bBbidroI99TnY0MA.png)
  

  
### 6. reduce

  It executes provided reducer (callback) method for each element of the given array, in the same order and passes the return value of the previous function call, in the function call of the current element as the first argument for the reducer function.

  - For the first reducer function call it will use the provided initial value (if provided)
  - Otherwise, use the `1st` element as the initial value and start calling the reducer function with the `2nd` element for the array
  
  ```javascript
  array.reduce((previousValue, currentValue, currentIndex, array) => { /* … */ }, initialValue)
  ```
  **Uses:**
  
  Example 1: 
  
  ![reduce Example 1](https://cdn-images-1.medium.com/max/800/1*0hVHugr84SNB_XbJBXK6CA.png)

  Example 2: 
  
  ![reduce Example 2](https://cdn-images-1.medium.com/max/800/1*hoalBfxQd6W-lj8Qxjh0KQ.png)

### 7. sort

  It sorts array elements and returns the same array (with sorted elements)

  - The default sort order is ascending.
  - For comparison: it first converts elements into strings and then compares their sequences of UTF-16 code unit values.
  
  ```javascript
  array.sort((a, b) => { /* … */ } )
  ```
  **Uses:**
  
  Example 1: 
  
  ![sort Example 1](https://cdn-images-1.medium.com/max/800/1*nP7MV6lEH3rzczz30f0PyA.png)

  Example 2: 
  
  ![sort Example 2](https://cdn-images-1.medium.com/max/800/1*tj8r0qquqe0s_ZKKUdCYLA.png)


### 8. forEach

  It executes the provided callback function for each element in the provided array.
  
  ```javascript
  array.forEach((element, index, array) => { /* … */ })
  ```
  **Uses:**
  
  Example 1: 
  
  ![forEach Example 1](https://cdn-images-1.medium.com/max/800/1*fjzejqgoIZf5rgYmgxMYCQ.png)

  Example 2: 
  
  ![forEach Example 2](https://cdn-images-1.medium.com/max/800/1*B0cc1X2J3jGWGOUTRP1ZRQ.png)

### 9. find

  It returns the first element that passes the test in the provided callback function (test function). if none of the elements passes the test, it returns `undefined`
  
  ```javascript
  array.find((element, index, array) => { /* … */ } )
  ```
  **Uses:**
  
  Example 1: 
  
  ![find Example 1](https://cdn-images-1.medium.com/max/800/1*6hqmeIHAdK4sfo-KwS-ibg.png)

  Example 2: 
  
  ![find Example 2](https://cdn-images-1.medium.com/max/800/1*HGKRMuurHyvCVbWRMHjdZw.png)

### 10. findIndex

  It returns the index of the first element that passes the test in the provided callback function (test function). if none of the elements passes the test, it returns -1
  
  ```javascript
  array.findIndex((element, index, array) => { /* … */ } )
  ```
  **Uses:**
  
  Example 1: 
  
  ![findIndex Example 1](https://cdn-images-1.medium.com/max/800/1*TDUj50Fo9i3P4rUtK5kvkg.png)

  Example 2: 
  
  ![findIndex Example 2](https://cdn-images-1.medium.com/max/800/1*9zJjRCm3zFID9NZ1AZqvag.png)

### Conclusion

Here we discussed some of the essential JS array methods, their syntax, a normal example, and an example with an array of objects.

If you have reached so far in this journey. You have gained a lot. 🔥 

Give a pat on your shoulder 😎

There are a lot of other important JS array methods, that are very useful in day-to-day programming. 

Let us know in the comments, what other JS array method you know. 💡

Peace out!

You can check the full list of array methods here: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
