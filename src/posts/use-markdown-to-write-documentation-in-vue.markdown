---
title:  "Use markdown to write documentation in Vue"
date:  "2018-10-17"
categories: vue
author: Shaina Sabarwal
image: "/publicImages/vue-documentation.jpeg"
comments: true
keywords: "vue, markdown, documentation, web development"
---

Markdown is simple and productive for writing notes, emails, todos, documentation and so on. These days almost every framework is have libraries to make static sites in markdown and publish as a blog or a documentation. 

React has gatsby, Rails has jekyll, and Vue has Vuepress. But what if you want to add a markdown code inside your vue project. Instead of generating a full fledged static site for documentation. I just needed to add a webpage with documentation for my client. The documentation was in markdown. So, I had to convert the markdown code into html inside the vue template and then style that html. It was a little tricky thing to do. Let's see, how I did it.

## Step 1. Converting markdown code into HTML inside vue template
I used a vue package [vue-markdown](https://www.npmjs.com/package/vue-markdown). It is really easy to use this package and convert your markdown code into html.
```html
<template>
    <div id="documentation">
        <vue-markdown>
            # Documentation
            *Documentation makes writing and reading code easy*
            ### Sub heading 1
            ### Sub heading 2
            Some more content for the documentation
            ...
            even bigger documentation
        </vue-markdown>
    </div>
</template
<script>
import VueMarkdown from 'vue-markdown';
export default {
    components:{
        vueMarkdown:VueMarkdown
    }
}
</script>
```

As you run the the above code and see the output in the browser, you will some something like below
```markdown
# Documentation
*Documentation makes writing and reading code easy*
### Sub heading 1
### Sub heading 2
Some more content for the documentation
...
even bigger documentation
```
Your markdown code just converted to HTML and published inside your vue documentation.

## Step 2: Styling the documentation
Now the documentation code is in markdown, what if I want to give the heading the theme color of our site, or give some margin or padding. For styling this markdown code, we need to actually understand this markdown code and parse it line by line into HTML ourself. So the above template code is basically get converted into following HTML code.
```html
<div id="documentation">
<h1> Documentation </h1>
<i> Documentation makes the writing and reading code easy </i>
<h3> Sub headng 1 </h3>
<h3> Sub heading 2 </h3>
Some more content for the documentation
...
even bigger documentaion
</div>
```
Now to style this documentation code, you need to add below styles
```css
<style>
    #documentation {
        color: #333;
        background-color: #fafafa;
        width: 70%;
        margin: auto;
    }
    #documentaion h1 {
        color: #5e4593
        test-align: center;
    }
    #documentation i {
        color: green;
        font-size: 12px;
    }
</style>
```
## Step 3: Use javascript to give class to HTML element converted from markdown & more.
```javascript
<script>
import VueMarkdown from 'vue-markdown';
export default {
    mounted: {
        const doc = document.querySelector('.documentation');
        const h3Tag = document.querySelector('.documentation h3')
        h3Tag.classList.add("first-subheading");
    },
    components:{
        vueMarkdown:VueMarkdown
    }
}
</script>
```
You can use the developer tools of the browser to see the converted HTML from the markdown code. 
Thank you :)

