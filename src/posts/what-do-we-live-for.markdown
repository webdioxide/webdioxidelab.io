---
title:  "What do we live for"
date:  "2018-10-17"
categories: design
author: Shaina Sabarwal
comments: true
keywords: "design, life"
---
I have been asked this question a lot many times "GSOC finished, what's next?" 

Most of the time, I replied working with Ishwerdas studios and few were I am going to learn UI/UX development because today, we are highly in need of good user interface. Most of the people responded like OK cool, good, wow, but one of them asked me a question – Who said you that design is more important? If something is working well, solving our purpose, then whether it is in green or blue, big or small in look, why do we care?

At the time, I was not able to reply him satisfactorily, I took me few days to think that yeah we need design but why?

Looking at the walls of my home, I got it colored beautifully, light blue curtains adding its beauty. I looked at the nature with such beautiful colors having different styles of trees, leaves, flowers, beautiful birds and I smiled.

The answer I got from these, this beauty is what we live for. In our lives, we have two types of things. Things we do to live, and things we live to do. Like we go to school, office, we pay our bills, we make a home. These are the things we do to live, but then there are other things live, we spend time with our family and friends, we love, romance, decorate our houses, cook delicious food, write and read poetry. Poetry, beauty, Romance is what we stay alive for. Check out this clip from [Dead poets society](https://www.youtube.com/watch?v=-VUV2Yl8gsI)

Today the back-end is matured enough, we have plugins, libraries, that works so wonderfully for us, with in few minutes, the app is ready with no pain. But what is in that going to make it your app, the idea, and how is it going to approach the user and make him smile or bring him tears, how is it gonna pass your feeling to him, and this is where front-end comes in, this is where Design comes in.

Next time you leaving your app only with back-end, look at your house or office and think, what if it is made up of bricks only with no paint, curtains and your favorite furniture?

*Orginally appeared at https://shainasabarwal.com/what-do-we-live-for/ *
