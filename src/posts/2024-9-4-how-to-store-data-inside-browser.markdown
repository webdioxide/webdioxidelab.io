---
layout: post
title: "How to Store Data Inside Browsers"
date: "2024-09-04"
categories: javascript
author: Simarjot Singh
image: "/publicImages/storage.jpg"
comments: true
keywords: "javascript, localStorage, browserStorage"
---

Have you ever wondered how to store data on the client side, i.e. directly in your browser?

Let’s say you a have web application that records how many times the user has visited your website or you have any game website that stores your last played game score. So if this information does not need to be stored at the backend then we will be using local storage.

## Introduction to LocalStorage
LocalStorage is a web storage mechanism that stores data on the client side within the browsers. The data is stored as key-value pairs, meaning that each piece of data is associated with a specific property (key).

For example: storing your last score of the last played game against the key named score. All data in LocalStorage is stored as a JSON String in key-value pairs.

Local storage has some methods which are listed below:

- `localStorage.setItem()`
- `localStorage.getItem()`
- `localStorage.removeItem()`
- `localStorage.clear()`
- `localStorage.key(index)`

## Viewing LocalStorage Data
Open the developer's tools and navigate to the Applications tab then on the left-hand side you will see the local storage and clicking on the same will show the data stored in it in the form of a key-value pair in the right panel.


## How to store data in local storage 
To set data or items inside the local storage we will use `localStorage.setItem(‘key’,’ value’)`. It requires two parameters: a Key and a value. The value is stored against the key in the local storage.

![set-item](/publicImages/set-item.png)

Now the value 50 will be stored against the key `score` in local storage like this:

![set-item-output](/publicImages/set-item-output.png)

## How to get the value from the local storage

To get the value from the local storage we have a method named `localStorage.getItem(‘key’)` 
It accepts 1 parameter i.e. key-name of the value you want to retrieve. Now in this case, if we want to retrieve the value of the key named `score` we will do it like this:

![get-item](/publicImages/get-item.png)

It will check if the score key exists in LocalStorage. If it does, the value will be stored in a variable; if not, null will be logged. In this case, it will log 50.

## How to get the key against the index
To get the key name associated with a specific index in LocalStorage we have a method named `localStorage.key(index)` which accepts 1 parameter i.e. the index 
number of the key you want in this case we will write our code like this: 

![key-index](/publicImages/key-index.png)
Now the key name score will be logged on the screen.

## How to remove data from the local storage
To delete any key pair from the local storage, use `localStorage.removeItem(‘key’)` method, accept 1 parameter i.e. the key name which is to be removed from the local storage. Now in this case, if we can remove the score key-value pair we will write it like this:

![remove-item](/publicImages/remove-item.png)

Now if the key name `score` exists in the local storage then it will be deleted. In this case, the key name `score` exists in the local storage then the item is removed from the local storage resulting the empty local storage in this case:

![remove-item-output](/publicImages/remove-item-output.png)

## How to clear local storage at once

To get all key-value pairs of local storage at once then we have to write the code like this:

![clear](/publicImages/clear.png)

It will remove all the key-value pairs from the local storage, which will result in empty local storage.

## How to store complex data in local storage
We can’t store complex data like arrays, objects and arrays of objects directly in local storage to store complex data types we have to use JSON.stringify() method which will convert all the complex data to the JSON string and it can be easily stored inside the local storage. For example we need to store the score of multiple games in an array we will store it like this:

![complex-data](/publicImages/local-storage-complex-data.png)

Here the array of [50,60,70,80] will stored in local storage against the key name score But it will only be applicable for the first time.

![complex-data-output](/publicImages/complex-data-output.png)

To update or add some more items in the same array we need to write code like this

![complex-data-update](/publicImages/complex-data-update.png)

Now we will have to get the array from local storage using JSON.parse() and we need to store it inside a variable and then add the other value to that variable or array and then we need to store it back inside the local storage using `localStorage.setItem()` and JSON.stringify.
