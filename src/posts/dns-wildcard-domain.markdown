---
layout: post
title:  "LetsEncrypt - Wildcard SSL and everything around it"
date: "2018-10-16"
categories: security
author: Vigas Deep
keyword: "HTTPS, SSL, Wildcard, LetsEncrypt"
comments: true
---

_What is a wildcard SSL certificate?_ In simple words, A SSL certificate which is valid for *.ExampleDomain.com and ExampleDomain.com as well, and * is the wildcard here, which means it can be anything in place of that *, very relateable.

 _Great! so how do we get one for free?_ Lets encrypt announced WildCard certificated in early 2018 and I'm gonna show you how you can get one for yourself and tips and tricks around it. This guide involves extensive use of Terminal.

## Lets do it

1. Install the [certbot](https://certbot.eff.org/).
2. Issue the following command after changing the both occurances of `exampledomain.io` with your own domain while keeping the format same.
    ```bash
    sudo certbot certonly --server https://acme-v02.api.letsencrypt.org/directory --manual --preferred-challenges dns -d 'exampledomain.io,*.exampledomain.io'
    ```
3. Since we are using DNS challenges to verify the authenticity that we really own the domain, certbot will ask us to add multiple TXT records in the Domain's DNS server configuration, which will look something like this.

![DNS CHALLENGE EXAMPLE](/publicImages/DNS-Challenge-example.png "DNS CHALLENGE EXAMPLE")
4. Add the TXT record, keep the TTL least as possible, fire up a new terminal instance and verify using following command ( again, change the domain name accordingly before firing )
    ```bash
    nslookup -q=TXT _acme-challenge.exampleDomain.com 8.8.8.8
    ```
    once you make sure, your changes are propagated and you can see them in result of above command, allow the certbot to proceed which was waiting for you to make changes.
5. Repeat step 4 if certbot asks for any more changes.

that's it lads, easy peasy.