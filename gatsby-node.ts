// Because we used ts-node in gatsby-config.js, this file will automatically be
// imported by Gatsby instead of gatsby-node.js.

// Use the type definitions that are included with Gatsby.
import { GatsbyNode } from 'gatsby';
import { createFilePath } from 'gatsby-source-filesystem';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');

export const onCreateNode: GatsbyNode['onCreateNode'] = async ({
  actions, node, getNode,
}) => {
  const { createNodeField } = actions;
  if (node.internal.type === 'MarkdownRemark') {
    const slug = createFilePath({ node, getNode, basePath: 'posts' });
    createNodeField({
      node,
      name: 'slug',
      value: slug,
    });
  }
};

interface MarkdownRemark {
  allMarkdownRemark: {
    edges: {
      node: {
        fields: {
          slug: string
        }
      }
    }[]
  }
}

export const createPages: GatsbyNode['createPages'] = async ({
  actions,
  graphql,
}) => {
  const { createPage } = actions;
  return new Promise((resolve, reject) => {
    graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
    `).then((result) => {
      (result.data as MarkdownRemark).allMarkdownRemark.edges.forEach(({ node }) => {
        createPage({
          path: node.fields.slug,
          component: path.resolve('./src/templates/blog-post.tsx'),
          context: {
            // Data passed to context is available in page queries as GraphQL variables.
            slug: node.fields.slug,
          },
        });
      });
      resolve();
    })
      .catch(() => reject());
  });
};
